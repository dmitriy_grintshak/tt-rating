package com.tt.ratingcalculator.service;

import com.tt.ratingcalculator.api.dto.UserDto;

import java.math.BigDecimal;

public interface RatingCalculationService {
    BigDecimal calculateRating(UserDto userDto, BigDecimal calculationSeed);
}