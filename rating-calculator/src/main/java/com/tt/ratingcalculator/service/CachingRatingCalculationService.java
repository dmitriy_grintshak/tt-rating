package com.tt.ratingcalculator.service;

import com.tt.ratingcalculator.api.dto.UserDto;
import com.tt.ratingcalculator.configuration.RedisConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service("cachingRatingCalculationService")
@Slf4j
@RequiredArgsConstructor
public class CachingRatingCalculationService implements RatingCalculationService {
    private final RedisTemplate<String, UserDto> redisTemplate;

    private final RatingCalculationService ratingCalculationService;

    @Override
    public BigDecimal calculateRating(UserDto userDto, BigDecimal calculationSeed) {
        return getCachedRating(userDto).orElseGet(() -> calculateAndCacheRating(userDto, calculationSeed));
    }

    private BigDecimal calculateAndCacheRating(UserDto userDto, BigDecimal calculationSeed) {
        BigDecimal rating = ratingCalculationService.calculateRating(userDto, calculationSeed);

        try {
            redisTemplate.opsForHash().put(RedisConfiguration.RATING_CACHE_NAME, userDto, rating);

            log.debug("Cached the rating of {} points for the user {}.", rating, userDto);
        } catch (Exception e) {
            log.warn("Exception when storing the rating for {}.", userDto, e);
        }

        return rating;
    }

    private Optional<BigDecimal> getCachedRating(UserDto userDto) {
        HashOperations<String, UserDto, BigDecimal> hashOperations = redisTemplate.opsForHash();

        BigDecimal rating = null;
        try {
            rating = hashOperations.get(RedisConfiguration.RATING_CACHE_NAME, userDto);
        } catch (Exception e) {
            log.warn("Exception when loading the cached rating for {}.", userDto, e);
        }

        log.debug("Found the cached rating {} for user {}.", rating, userDto);

        return Optional.ofNullable(rating);
    }
}