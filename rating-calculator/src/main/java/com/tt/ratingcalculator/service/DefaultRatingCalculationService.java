package com.tt.ratingcalculator.service;

import com.tt.ratingcalculator.api.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service("ratingCalculationService")
@Slf4j
public class DefaultRatingCalculationService implements RatingCalculationService {
    @Override
    public BigDecimal calculateRating(UserDto userDto, BigDecimal calculationSeed) {
        BigDecimal rating = calculationSeed.multiply(BigDecimal.valueOf(userDto.getAge()));

        log.debug("Calculated the rating of {} points for the user {}.", rating, userDto);

        return rating;
    }
}