package com.tt.ratingcalculator.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RatingCalculationRequestDto {
    @NotNull
    private BigDecimal calculationSeed;

    @Valid
    private UserDto user;
}
