package com.tt.ratingcalculator.api.listener;

import com.tt.ratingcalculator.api.dto.RatingCalculationRequestDto;
import com.tt.ratingcalculator.api.dto.UserDto;
import com.tt.ratingcalculator.configuration.KafkaConfiguration;
import com.tt.ratingcalculator.service.RatingCalculationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.math.BigDecimal;

@Component
@Slf4j
public class UserRatingTopicListener {
    private final RatingCalculationService ratingCalculationService;

    @Autowired
    public UserRatingTopicListener(
            @Qualifier("cachingRatingCalculationService") RatingCalculationService ratingCalculationService) {
        this.ratingCalculationService = ratingCalculationService;
    }

    @KafkaListener(topics = KafkaConfiguration.USER_RATING_TOPIC_NAME)
    public void onMessage(@Payload @Valid RatingCalculationRequestDto request) {
        log.debug("Received request: {}.", request);

        UserDto user = request.getUser();

        BigDecimal rating = ratingCalculationService.calculateRating(user, request.getCalculationSeed());

        log.info("The rating is {} points for the user {} {} ({} years old).",
                rating, user.getFirstName(), user.getLastName(), user.getAge());
    }
}