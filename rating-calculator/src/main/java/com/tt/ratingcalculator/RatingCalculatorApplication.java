package com.tt.ratingcalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingCalculatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(RatingCalculatorApplication.class, args);
    }
}