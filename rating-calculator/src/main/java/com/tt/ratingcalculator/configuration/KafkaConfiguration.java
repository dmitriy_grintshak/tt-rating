package com.tt.ratingcalculator.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListenerConfigurer;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerEndpointRegistrar;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.util.backoff.FixedBackOff;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@EnableKafka
public class KafkaConfiguration implements KafkaListenerConfigurer {
    public static final String USER_RATING_TOPIC_NAME = "user.rating";
    private static final int USER_RATING_TOPIC_PARTITIONS_COUNT = 4;
    private static final int USER_RATING_TOPIC_REPLICAS_COUNT = 1;

    @Autowired
    private LocalValidatorFactoryBean validator;

    @Override
    public void configureKafkaListeners(KafkaListenerEndpointRegistrar registrar) {
        registrar.setValidator(validator);
    }

    /**
     * This is needed solely to set the error handler.
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<?, ?> kafkaListenerContainerFactory(
            ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
            ConsumerFactory<Object, Object> kafkaConsumerFactory,
            ErrorHandler errorHandler) {
        ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();

        configurer.configure(factory, kafkaConsumerFactory);

        factory.setErrorHandler(errorHandler);

        return factory;
    }

    /**
     * @return An ErrorHandler, that simply logs the failed messages after the first attempt.
     */
    @Bean
    public ErrorHandler errorHandler() {
        return new SeekToCurrentErrorHandler(new FixedBackOff(0, 0));
    }

    @Bean
    public RecordMessageConverter recordMessageConverter() {
        return new StringJsonMessageConverter();
    }

    @Bean
    public NewTopic userRatingTopic() {
        return TopicBuilder
                .name(USER_RATING_TOPIC_NAME)
                .partitions(USER_RATING_TOPIC_PARTITIONS_COUNT)
                .replicas(USER_RATING_TOPIC_REPLICAS_COUNT)
                .build();
    }
}
