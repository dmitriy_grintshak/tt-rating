package com.tt.ratingcalculator.service;

import com.tt.ratingcalculator.api.dto.UserDto;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class DefaultRatingCalculationServiceTest {
    private DefaultRatingCalculationService ratingCalculationService = new DefaultRatingCalculationService();

    @Test
    public void shouldCalculateRating() {
        BigDecimal calculationSeed = BigDecimal.valueOf(0.1d);
        UserDto userDto = UserDto.builder().age(35).build();

        BigDecimal rating = ratingCalculationService.calculateRating(userDto, calculationSeed);

        assertThat(rating, is(equalTo(BigDecimal.valueOf(3.5d))));
    }
}