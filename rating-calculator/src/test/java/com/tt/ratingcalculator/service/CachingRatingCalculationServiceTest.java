package com.tt.ratingcalculator.service;

import com.tt.ratingcalculator.api.dto.UserDto;
import com.tt.ratingcalculator.configuration.RedisConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CachingRatingCalculationServiceTest {
    @Mock
    private DefaultRatingCalculationService defaultRatingCalculationService;

    @Mock
    private HashOperations<String, Object, Object> hashOperations;

    @Mock
    private RedisTemplate<String, UserDto> redisTemplate;

    @InjectMocks
    private CachingRatingCalculationService ratingCalculationService;

    @Before
    public void setUp() {
        given(redisTemplate.opsForHash()).willReturn(hashOperations);
    }

    @Test
    public void shouldReturnCachedRatingIfExists() {
        given(hashOperations.get(any(), any())).willReturn(BigDecimal.ONE);

        BigDecimal calculatedRating = ratingCalculationService.calculateRating(UserDto.builder().build(), BigDecimal.ZERO);

        verifyZeroInteractions(defaultRatingCalculationService);

        assertThat(calculatedRating, is(equalTo(BigDecimal.ONE)));
    }

    @Test
    public void shouldCalculateAndCacheRatingIfNotCached() {
        UserDto userDto = UserDto.builder().build();
        BigDecimal calculationSeed = BigDecimal.ZERO;
        BigDecimal rating = BigDecimal.ONE;

        given(hashOperations.get(any(), any())).willReturn(null);
        given(defaultRatingCalculationService.calculateRating(any(), any())).willReturn(rating);

        BigDecimal calculatedRating = ratingCalculationService.calculateRating(userDto, calculationSeed);

        verify(defaultRatingCalculationService).calculateRating(eq(userDto), eq(calculationSeed));
        verify(hashOperations).put(eq(RedisConfiguration.RATING_CACHE_NAME), eq(userDto), eq(rating));

        assertThat(calculatedRating, is(equalTo(rating)));
    }

    @Test
    public void shouldCalculateRatingIfCacheLoadingFails() {
        UserDto userDto = UserDto.builder().build();
        BigDecimal calculationSeed = BigDecimal.ZERO;
        BigDecimal rating = BigDecimal.ONE;

        given(hashOperations.get(any(), any())).willThrow(Exception.class);
        given(defaultRatingCalculationService.calculateRating(any(), any())).willReturn(rating);

        BigDecimal calculatedRating = ratingCalculationService.calculateRating(userDto, calculationSeed);

        verify(defaultRatingCalculationService).calculateRating(eq(userDto), eq(calculationSeed));
        verify(hashOperations).put(eq(RedisConfiguration.RATING_CACHE_NAME), eq(userDto), eq(rating));

        assertThat(calculatedRating, is(equalTo(rating)));
    }

    @Test
    public void shouldCalculateAndReturnRatingIfCacheStoringFails() {
        UserDto userDto = UserDto.builder().build();
        BigDecimal calculationSeed = BigDecimal.ZERO;
        BigDecimal rating = BigDecimal.ONE;

        willThrow(Exception.class).given(hashOperations).put(any(), any(), any());
        given(defaultRatingCalculationService.calculateRating(any(), any())).willReturn(rating);

        BigDecimal calculatedRating = ratingCalculationService.calculateRating(userDto, calculationSeed);

        verify(defaultRatingCalculationService).calculateRating(eq(userDto), eq(calculationSeed));

        assertThat(calculatedRating, is(equalTo(rating)));
    }
}