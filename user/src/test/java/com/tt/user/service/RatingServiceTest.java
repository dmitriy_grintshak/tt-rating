package com.tt.user.service;

import com.tt.user.configuration.KafkaConfiguration;
import com.tt.user.domain.User;
import com.tt.user.integration.ratingcalculator.dto.RatingCalculationRequestDto;
import com.tt.user.integration.ratingcalculator.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.concurrent.ListenableFuture;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceTest {
    private static final BigDecimal CALCULATION_SEED = BigDecimal.ONE;

    @Mock
    private KafkaTemplate<Object, Object> template;

    private RatingService ratingService;

    @Before
    public void setUp() {
        ratingService = new RatingService(template, CALCULATION_SEED);

        given(template.send(any(), any())).willReturn(mock(ListenableFuture.class));
    }

    @Test
    public void shouldSendMessageToKafkaTopic() {
        String firstName = "FirstName";
        String lastName = "LastName";
        int age = 35;
        User user = User.builder().firstName(firstName).lastName(lastName).age(age).build();

        ratingService.calculateRating(user);

        RatingCalculationRequestDto expectedRatingCalculationRequestDto = RatingCalculationRequestDto
                .builder()
                .calculationSeed(CALCULATION_SEED)
                .user(UserDto
                        .builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .age(age)
                        .build())
                .build();

        verify(template).send(eq(KafkaConfiguration.USER_RATING_TOPIC_NAME), eq(expectedRatingCalculationRequestDto));
    }
}