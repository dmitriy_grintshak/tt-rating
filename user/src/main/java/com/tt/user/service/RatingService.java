package com.tt.user.service;

import com.tt.user.domain.User;
import com.tt.user.integration.ratingcalculator.dto.RatingCalculationRequestDto;
import com.tt.user.integration.ratingcalculator.dto.UserDto;
import com.tt.user.configuration.KafkaConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
public class RatingService {
    private final KafkaTemplate<Object, Object> template;

    private final BigDecimal calculationSeed;

    @Autowired
    public RatingService(
            KafkaTemplate<Object, Object> template,
            @Value("${ratingService.calculationSeed}") BigDecimal calculationSeed) {
        this.template = template;
        this.calculationSeed = calculationSeed;
    }

    public void calculateRating(User user) {
        UserDto userDto = UserDto
                .builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .age(user.getAge())
                .build();
        RatingCalculationRequestDto ratingCalculationRequestDto = RatingCalculationRequestDto
                .builder()
                .user(userDto)
                .calculationSeed(calculationSeed)
                .build();

        template
                .send(KafkaConfiguration.USER_RATING_TOPIC_NAME, ratingCalculationRequestDto)
                .addCallback(
                        result -> log.debug("Message sent to the topic {}.", KafkaConfiguration.USER_RATING_TOPIC_NAME),
                        t -> log.error("Could not send the message to the topic {}.", KafkaConfiguration.USER_RATING_TOPIC_NAME, t));
    }
}