package com.tt.user.service;

import com.tt.user.domain.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final RatingService ratingService;

    public void createUser(User user) {
        ratingService.calculateRating(user);
    }
}