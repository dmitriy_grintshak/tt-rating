package com.tt.user.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaConfiguration {
    public static final String USER_RATING_TOPIC_NAME = "user.rating";
    private static final int USER_RATING_TOPIC_PARTITIONS_COUNT = 4;
    private static final int USER_RATING_TOPIC_REPLICAS_COUNT = 1;

    @Bean
    public NewTopic userRatingTopic() {
        return TopicBuilder
                .name(USER_RATING_TOPIC_NAME)
                .partitions(USER_RATING_TOPIC_PARTITIONS_COUNT)
                .replicas(USER_RATING_TOPIC_REPLICAS_COUNT)
                .build();
    }
}