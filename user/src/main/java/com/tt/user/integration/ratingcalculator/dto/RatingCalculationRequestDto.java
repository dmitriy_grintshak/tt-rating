package com.tt.user.integration.ratingcalculator.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class RatingCalculationRequestDto {
    private BigDecimal calculationSeed;
    private UserDto user;
}