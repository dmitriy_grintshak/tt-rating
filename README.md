# Software Engineer task solution #

## Prerequisites ##
- JDK v. 8.
- Docker (I used v. 2.2).

## General description ##
The original task is formulated very abstractly. So I assumed the first microservice is a user
registry, and the second one is a ratings registry.

Two microservices were implemented: _user_ and _rating-calculator_.
Both use Spring Boot and are connected using Kafka.

The _user_ microservice provides a REST API at /api/v1/user. It accepts POST queries with
JSON payload like
```
{
 	"first_name": "FirstName",
 	"last_name": "LastName",
 	"age": "30"
 }
```
The data is validated and converted into the Kafka payload to be handled by the _rating-calculator_
microservice.

The _rating-calculator_ microservice is listening on the topic `user.rating`.
The incoming message is validated and the rating is loaded from the cache, or calculated.
The calculated rating is cached (I used the whole user data as a cache key, as there is no better
unique identifier). The cache is not cleaned. In case the cache is not available, the rating is
calculated anew.
The result of the rating calculation is logged down at the INFO level in the form
```
The rating is 15.0 points for the user FirstName LastName (30 years old).
```

The messages, that cannot be handled (format, validation, etc.), are simply logged down. They do
not get to the unhandled messages topic.

Also, the topic configuration is hardcoded along with its properties (the KafkaConfiguration class),
and the configuration for ensuring the topic exists is present in both microservices
(not so good, but the alternative is to either have a startup dependency, or create the topic
externally).

## How to build and run the services ##
The following assumes to be executed from the **respective projects** folders.

Use `./mvnw clean package` to build the applications.

Create the containers: `docker build -t user .`, `docker build -t rating-calculator .`.

The `wait` utility is embedded into the containers, for Kafka and Redis to start listening on the respective ports,
before the applications containers start (mainly needed for Kafka topics creation on the startup, as there will be
only N attemps made, and Kafka may not have started by then). See
https://www.datanovia.com/en/lessons/docker-compose-wait-for-container-using-wait-tool/.

The following assumes to be executed from the **/docker** folder.

There are two docker-compose configurations available:

* default (`docker-compose.yml`), starts a cluster of Zookeeper, 2 instances of Kafka, Redis, _user_ and 2
instances of _rating-calculator_. Only the port 8080 is forwarded for the API to be accessible. It also expects the
certain environment variables, like the Kafka and Redis URLs and the calculation seed, to be given along,
use the `environment.properties` file for that.
* development (`docker-compose-development.yml`), starts Zookeeper, 2 instances of Kafka, Redis. The Kafka and
the Redis ports are forwarded. Supplies the Kafka and Redis dependencies for the development purposes.

For the demo purposes, use the default cluster: `docker-compose --env-file environment.properties up`.

Assuming the port 8080 is forwarded to the local port 8080, the Create User API call has to
be pointed at `http://localhost:8080/api/v1/user`.

For the development purposes in the IDE, the expected configuration variables are also available under Spring
profile `development`, see the files `application-development.properties` under the projects resources folders.

## What is not covered ##
* Message serialisation / de-serialisation integration tests. There exists a unit test for the message
sending case.
* The same for the REST controller tests - almost no logic in the controller.
* I did not go on with any real security configuration in regards to Kafka.
* Cache cleaning (only available via starting the clean Redis server). This would allow to recalculate
the ratings, in case the seed has changed.